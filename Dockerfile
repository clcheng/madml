FROM tensorflow/tensorflow:latest-gpu-py3

LABEL maintainer="chi.lung.cheng@cern.ch"
ARG NCORES=2
ARG GIT_TAG

RUN apt update \
  && apt install -y \
    git dpkg-dev cmake g++ gcc binutils libx11-dev libxpm-dev \
    libxft-dev libxext-dev \
    gfortran libssl-dev libpcre3-dev \
    xlibmesa-glu-dev libglew1.5-dev libftgl-dev \
    libmysqlclient-dev libfftw3-dev libcfitsio-dev \
    graphviz-dev libavahi-compat-libdnssd-dev \
    libldap2-dev libxml2-dev libkrb5-dev \
    libgsl0-dev libqt4-dev python python3 python3-dev python3-pip \
  && rm -rf /var/lib/apt/lists/* \
  && pip3 install ipython

RUN cd /opt \
  && git clone http://github.com/root-project/root.git root_source \
  && cd root_source \
  && git checkout ${GIT_TAG} \
  && rm -rf /opt/root_source/.git

WORKDIR /opt/root
RUN cmake -Dpython=ON -DPYTHON_EXECUTABLE=/usr/bin/python3 ../root_source 
RUN make -j ${NCORES}
RUN useradd -m user && chmod a+w /home/user
USER user

WORKDIR /home/user

ADD entry_point.sh /usr/local/bin/entry_point.sh
ENTRYPOINT ["/usr/local/bin/entry_point.sh"]
